<?php

namespace Shopware\SegPlzVerifizieren\Subscriber;

use \Shopware\SegPlzVerifizieren\Components;

/**
 * @SuppressWarnings(PHPMD.CamelCaseClassName)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Frontend implements \Enlight\Event\SubscriberInterface
{
    /* ISO code des Landes, fuer das die PLZ geprueft werden soll */
    const COUNTRYISO = 'DE';

    /* the Snippet Namespace */
    protected $_snippets;

    public static function getSubscribedEvents()
    {
        return array(
            'Shopware_Controllers_Frontend_Register::validatePersonal::after'
            => 'afterValidatePersonal'
        );
    }

    /**
     * Beschweren, wenn die PLZ nicht wie eine PLZ aussieht.
     */
    public function afterValidatePersonal(Enlight_Hook_HookArgs $arguments)
    {
        $retval = $arguments->getReturn();
        $errorMessages = $retval['sErrorMessages'];
        $errorFlag = $retval['sErrorFlag'];

        $controller = $arguments->getSubject();
        $postdata = $controller->Request()->getParam('register');
        $billing = $postdata['billing'];

        if (empty($billing['country'])) return;
        try {
            $verifyCountryId = $this->getCountryId();
        } catch (Components\InvalidCountryIsoException $e) {
            Shopware()->Pluginlogger()
                ->Info("Kein Land mit Laenderkennung DE in der Datenbank.");
            return;
        }
        $countryId = (int) $billing['country'];
        if ($verifyCountryId != $countryId) return;

        if ((!$this->isPlzValid($billing))
            || (!empty($billing['shippingAddress'])
                && !$this->isPlzValid($postdata['shipping']))) {
            $errorMessages[] = $this->getSnippet('plzpruefen');
            $errorFlag['zipcode'] = true;
        }

        $arguments->setReturn(
            [
                "sErrorMessages" => $errorMessages,
                "sErrorFlag" => $errorFlag
            ]
        );
    }

    /*
     * Holt die countryId aus der Datenbank
     */
    private function getCountryId()
    {
        $result = Shopware()->Db()->fetchOne(
            'select id from s_core_countries where countryiso = ?',
            array(self::COUNTRYISO)
        );
        if ($result === FALSE) {
            throw new Components\InvalidCountryIsoException(self::COUNTRYISO);
        }
        return (int) $result;
    }

    /**
     * Query the snippetmanager.
     */
    private function getSnippet($name)
    {
        if (!isset($this->_snippets)) {
            $this->_snippets = Shopware()->_snippets()
                ->getNamespace('frontend/segplzverifizieren/main');
        }
        return $this->_snippets->get($name);
    }

    private function isPlzValid($data)
    {
        if (empty($data['zipcode'])) return true;

        $zipcode = $data['zipcode'];
        $match = preg_match('/^\s*\d{5}\s*$/', $zipcode);
        if ($match === FALSE) {
            Shopware()->Pluginlogger()->Info(
                "Error matching this zipcode: " . $zipcode
            );
            return true;
        }
        if ($match === 1) return true;
        return false;
    }
}
