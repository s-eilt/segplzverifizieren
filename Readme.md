SegPlzVerifizieren
==================

Note to international users
---------------------------

This is a Shopware plugin to verify new customers in Germany enter a valid
German post code. If you are interested in translating this project or
extending it I will be happy to accept patches. For the license, see LICENSE.

Was ist das?
------------

Ein Shopware-Plugin, das versucht, die Postleitzahl beim Anlegen eines
Kundenkontos zu verifizieren. Speziell wird das Formular nicht angenommen und
eine Fehlermeldung produziert, wenn das Land Deutschland ist und die
Postleitzahl nicht aus genau 5 Ziffern besteht.

Dadurch soll verhindert werden, dass Kunden die Felder PLZ und Ort vertauschen.

Was ist das nicht?
------------------

In Kürze:

- konfigurierbar
- übersetzt
- Bug-frei

Idealerweise wären Land und PLZ-Format im Backend einstellbar, oder sogar eine
Konfiguration pro Land… und wenn man schon so weit denkt, müsste eigentlich
auch eine Übersetzung denkbar sein.

Patches werden gerne angenommen.

Kompatibilität
--------------

Der Code wird getestet mit Shopware 4.3 und 5.0, und benötigt PHP ab 5.4.

Wo bekommt man das?
-------------------

Das git-Repository liegt unter:
https://bitbucket.org/s-eilt/segplzverifizieren.git

Installation
------------

Zur Installation das Repository einfach in das Plugin-Verzeichnis von Shopware
klonen. Dabei ist zu beachten, dass Shopware Wert auf Groß- und Kleinschreibung
legt:

    cd engine/Shopware/Plugins/Local/Frontend
    git clone https://bitbucket.org/s-eilt/segplzverifizieren.git \
        SegPlzVerifizieren

Danach kann das Plugin im Shop-Backend installiert werden.

Lizenz
------

Siehe LICENSE in diesem Repository.

Autor
-----

Simon Eilting <simon@eilting.net>
