<?php

/**
 * @SuppressWarnings(PHPMD.CamelCaseClassName)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Shopware_Plugins_Frontend_SegPlzVerifizieren_Bootstrap
    extends Shopware_Components_Plugin_Bootstrap
{
    const SUPPORT = "simon@eilting.net";

    protected $_pluginInfo;

    public function __construct($name, $info=null)
    {
        $this->_pluginInfo = json_decode(
            file_get_contents(__DIR__ . DIRECTORY_SEPARATOR .'plugin.json'),
            true
        ); 
        parent::__construct($name, $info);
    }

    public function getCapabilities()
    {
        return array(
            'install' => true,
            'update' => false,
            'enable' => true
        );
    }
    
    public function getVersion()
    {
        return $this->_pluginInfo['currentVersion'];
    }

    public function getLabel()
    {
        return $this->_pluginInfo['label']['de'];
    }

    public function getInfo()
    {
        return array_merge(
            $this->_pluginInfo,
            array(
                'label' => $this->getLabel(),
                'support' => static::SUPPORT
            )
        );
    }

    public function install()
    {
        $minVersion = $this->_pluginInfo['compatibility']['minimumVersion'];
        if (!$this->assertVersionGreaterThen($minVersion)) {
            throw new \RuntimeException(
                'At least Shopware ' . $minVersion . ' is required'
            );
        }

        $this->subscribeEvent(
            'Enlight_Controller_Front_DispatchLoopStartup',
            'onStartDispatch'
        );

        $this->update('0.0.2');

        return true;
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function onStartDispatch(Enlight_Event_EventArgs $args)
    {
        $this->Application()->Loader()
            ->registerNamespace('Shopware\SegPlzVerifizieren', $this->Path());

        $this->Application()->Events()->addSubscriber(
            new \Shopware\SegPlzVerifizieren\Subscriber\Frontend()
        );
    }

    public function uninstall()
    {
        return true;
    }

    /** @SuppressWarnings(PHPMD.UnusedFormalParameter) */
    public function update($oldversion)
    {
        switch ($oldversion) {
        default:
            Shopware()->Pluginlogger()->Error(
                'Update from version ' . $oldversion . ' not implemented.'
            );
            return false;
        //case '0.0.2':
            //no break;
        }
        return true;
    }
}
