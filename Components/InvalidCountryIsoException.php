<?php

namespace Shopware\SegPlzVerifizieren\Components;

class InvalidCountryIsoException extends \Exception
{
    protected $_countryIso;

    public function __construct($countryIso)
    {
        parent::__construct(
            'Kein Land mit diesem ISO-Code in der DB vorhanden'
        );
        $this->_countryIso = $countryIso;
    }
}
